package organization

import (
	"gitlab.com/reactive-potato/auth/repository"
	"gitlab.com/reactive-potato/auth/snowflakeid"
)

type service struct {
	repository repository.Repository
}

func NewService(repository repository.Repository) *service {
	return &service{repository}
}

func (service *service) AddOrganization(organization *Organization) error {
	var err error

	organization.Id_, err = snowflakeid.New()
	if err != nil {
		return err
	}

	return service.repository.Add(organization)
}

func (service *service) DeleteOrganization(userId snowflakeid.SnowflakeId) error {
	return service.repository.Delete(userId)
}
