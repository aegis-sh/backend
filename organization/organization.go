package organization

import (
	"gitlab.com/reactive-potato/auth/entity"
)

type Organization struct {
	entity.Entity
	Name string `json:"name"`
}
