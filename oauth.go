package main

import (
	"encoding/json"
	"log"
	"net/http"
)

func authorize(response http.ResponseWriter, request *http.Request) {
	response.Write([]byte("OAuth authorize"))
}

func token(response http.ResponseWriter, request *http.Request) {
	response.Write([]byte("OAuth token"))
}

type AuthorizationServerMetadata struct {
	Issuer                string `json:"issuer"`
	AuthorizationEndpoint string `json:"authorization_endpoint"`
	TokenEndpoint         string `json:"token_endpoint"`
}

func authorizationServer(response http.ResponseWriter, request *http.Request) {
	authorizationServerMetadata := &AuthorizationServerMetadata{
		Issuer:                "http://localhost:8080",
		AuthorizationEndpoint: "http://localhost:8080/ouath/v2/authorize",
		TokenEndpoint:         "http://localhost:8080/ouath/v2/token",
	}

	authorizationServerMetadataJson, err := json.Marshal(authorizationServerMetadata)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		log.Default().Println(err)
		return
	}

	response.Header().Add("Content-Type", "application/json; charset=utf-8")
	response.Write(authorizationServerMetadataJson)
}

func loadOauthRoutes(router *http.ServeMux) {
	router.HandleFunc("GET /oauth/v2/authorize", authorize)
	router.HandleFunc("GET /oauth/v2/token", token)

	router.HandleFunc("GET /.well-known/oauth-authorization-server", authorizationServer)
}
