package snowflakeid

import (
	"strconv"
	"sync"
	"time"
)

const timestampBitLength = 41
const datacenterIdBitLength = 5
const workerIdBitLength = 5
const sequenceBitLength = 12

const timestampBitMask = 1<<timestampBitLength - 1
const sequenceBitMask = 1<<sequenceBitLength - 1

const timestampShift = datacenterIdBitLength + workerIdBitLength + sequenceBitLength
const datacenterIdShift = workerIdBitLength + sequenceBitLength
const workerIdShift = sequenceBitLength

var (
	mutex sync.Mutex

	epoch int64 = 1288834974657

	datacenterId uint8
	workerId     uint8

	lastTimestamp int64

	sequence uint16
)

func SetEpoch(epoch_ time.Time) {
	epoch = epoch_.UnixMilli()
}

func SetDatacenterId(id uint8) {
	datacenterId = id
}

func SetWorkerId(id uint8) {
	workerId = id
}

type SnowflakeId int64

func New() (SnowflakeId, error) {
	mutex.Lock()
	defer mutex.Unlock()

	timestamp := time.Now().UnixMilli()

	if timestamp == lastTimestamp {
		sequence = (sequence + 1) & sequenceBitMask
		if sequence == 0 {
			for timestamp <= lastTimestamp {
				timestamp = time.Now().UnixMilli()
			}
		}
	} else {
		sequence = 0
	}

	lastTimestamp = timestamp

	return SnowflakeId(
			((timestamp-epoch)&timestampBitMask)<<timestampShift |
				int64(datacenterId)<<datacenterIdShift |
				int64(workerId)<<workerIdShift |
				int64(sequence),
		),
		nil
}

func (snowflakeId SnowflakeId) Int64() int64 {
	return int64(snowflakeId)
}

func (snowflakeId SnowflakeId) String() string {
	return strconv.FormatInt(snowflakeId.Int64(), 10)
}
