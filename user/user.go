package user

import (
	"gitlab.com/reactive-potato/auth/entity"
)

type User struct {
	entity.Entity
	Name         string `json:"name"`
	Username     string `json:"username"`
	PasswordHash string `json:"password_hash"`
}
