package user

import (
	"gitlab.com/reactive-potato/auth/repository"
	"gitlab.com/reactive-potato/auth/snowflakeid"
)

type service struct {
	repository repository.Repository
}

func NewService(repository repository.Repository) *service {
	return &service{repository}
}

func (service *service) AddUser(user *User) error {
	var err error

	user.Id_, err = snowflakeid.New()
	if err != nil {
		return err
	}

	return service.repository.Add(user)
}

func (service *service) GetUser(userId snowflakeid.SnowflakeId) (*User, error) {
	user := new(User)

	err := service.repository.Get(userId, user)
	if err != nil {
		return nil, err
	}

	return user, err
}

func (service *service) DeleteUser(userId snowflakeid.SnowflakeId) error {
	return service.repository.Delete(userId)
}
