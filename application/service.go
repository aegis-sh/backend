package application

import (
	"gitlab.com/reactive-potato/auth/repository"
	"gitlab.com/reactive-potato/auth/snowflakeid"
)

type service struct {
	repository repository.Repository
}

func NewService(repository repository.Repository) *service {
	return &service{repository}
}

func (service *service) AddApplication(application *Application) (*Application, error) {
	var err error

	application.Id_, err = snowflakeid.New()
	if err != nil {
		return nil, err
	}

	err = service.repository.Add(application)

	return application, err
}

func (service *service) DeleteApplication(userId snowflakeid.SnowflakeId) error {
	return service.repository.Delete(userId)
}
