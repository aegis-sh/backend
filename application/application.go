package application

import "gitlab.com/reactive-potato/auth/entity"

type Application struct {
	entity.Entity
	Name string `json:"name"`
}
