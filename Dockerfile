FROM cgr.dev/chainguard/go:latest AS builder
WORKDIR /app
COPY . .
RUN go build -o auth .

FROM cgr.dev/chainguard/glibc-dynamic
COPY --from=builder /app/auth /usr/bin/
CMD ["/usr/bin/auth"]
