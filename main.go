package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/redis/go-redis/v9"
	"gitlab.com/reactive-potato/auth/repository"
	"gitlab.com/reactive-potato/auth/snowflakeid"
	"gitlab.com/reactive-potato/auth/user"
)

func init() {
	snowflakeid.SetEpoch(time.Date(2024, time.May, 13, 0, 0, 0, 0, time.UTC))
}

func main() {
	options, err := redis.ParseURL("redis://127.0.0.1:6379")
	if err != nil {
		panic(err)
	}

	client := redis.NewClient(options)

	userService := user.NewService(repository.NewRedisRepository(client, "user"))

	if err = userService.AddUser(&user.User{Name: "John Doe"}); err != nil {
		panic(err)
	}

	router := http.NewServeMux()

	loadOauthRoutes(router)

	loadOidcRoutes(router)

	server := http.Server{
		Addr:    ":8080",
		Handler: router,
	}

	fmt.Println("Server listening on :8080")
	server.ListenAndServe()
}
