package repository

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/redis/go-redis/v9"
	"gitlab.com/reactive-potato/auth/entity"
	"gitlab.com/reactive-potato/auth/snowflakeid"
)

type redisRepository struct {
	client *redis.Client
	prefix string
}

func NewRedisRepository(client *redis.Client, prefix string) Repository {
	return &redisRepository{client, prefix}
}

func (repository *redisRepository) Add(Identifiable entity.Identifiable) error {
	userJson, err := json.Marshal(Identifiable)
	if err != nil {
		return err
	}

	return repository.client.JSONSet(
		context.TODO(),
		fmt.Sprintf("%s:%d", repository.prefix, Identifiable.Id()),
		"$",
		userJson,
	).Err()
}

func (repository *redisRepository) Get(id snowflakeid.SnowflakeId, entity any) error {
	command := repository.client.JSONGet(context.TODO(), fmt.Sprintf("%s:%d", repository.prefix, id), "$")

	entitiesJson, err := command.Result()
	if err != nil {
		return err
	}

	var entities []json.RawMessage
	err = json.Unmarshal([]byte(entitiesJson), &entities)
	if err != nil {
		return err
	}

	return json.Unmarshal(entities[0], entity)
}

func (repository *redisRepository) Delete(id snowflakeid.SnowflakeId) error {
	return repository.client.JSONDel(
		context.TODO(),
		fmt.Sprintf("%s:%d", repository.prefix, id),
		"$",
	).Err()
}
