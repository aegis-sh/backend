package repository

import (
	"gitlab.com/reactive-potato/auth/entity"
	"gitlab.com/reactive-potato/auth/snowflakeid"
)

type Repository interface {
	Add(Identifiable entity.Identifiable) error
	Get(id snowflakeid.SnowflakeId, entity any) error
	Delete(id snowflakeid.SnowflakeId) error
}
