package entity

import (
	"gitlab.com/reactive-potato/auth/snowflakeid"
)

type Identifiable interface {
	Id() snowflakeid.SnowflakeId
}

type Entity struct {
	Id_ snowflakeid.SnowflakeId `json:"id"`
}

func (entity *Entity) Id() snowflakeid.SnowflakeId {
	return entity.Id_
}
